FROM node:18-alpine
WORKDIR /app
COPY frontend frontend
COPY server server
COPY requirements.txt requirements.txt
COPY launch.sh launch.sh

# Build React/Vite app
WORKDIR /app/frontend
RUN npm install
RUN npm run build

# Install python dependencies
RUN apk add python3
RUN apk add py3-pip
# Need git for fetching the right dev version of yt-downloader
RUN apk add git
WORKDIR /app
RUN pip install -r requirements.txt --break-system-packages

# Set up crontab for automatic playlist refreshing
RUN echo "0    *    *    *    *    python3 /app/server/cli.py playlist sync >> /data/crontab.log" | crontab -

EXPOSE 5000
CMD sh launch.sh &> /data/log.log