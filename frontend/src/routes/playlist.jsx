import { useLoaderData } from "react-router-dom";
import { getPlaylistData, getVideosForPlaylist } from "../util/api";
import { Link } from "react-router-dom";
import VideoCard from "../components/videocard";
import Stack from '@mui/joy/Stack'
import Typography from "@mui/joy/Typography/Typography";

export async function loader({ params }) {
    const playlistData = await getPlaylistData(params.playlistId);
    const videos = await getVideosForPlaylist(params.playlistId);
    return { playlistData, videos }
}

export default function PlaylistView() {
    const { playlistData, videos } = useLoaderData();

    if (playlistData.error) {
        return <>
            <h1>Invalid playlist!</h1>
            <Link to='/'>Go back to the main page</Link>
        </>
    }

    //console.log(videos);

    return <>
        <Typography level="h2">{playlistData.title} ({playlistData.num_videos} videos)</Typography>
        <Stack spacing={2} sx={{marginTop: 2}}>
        {videos.map((data, idx) => <VideoCard {...data} key={data.id}/>)}
        </Stack>  
    </>
}