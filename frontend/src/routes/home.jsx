import { useState, useEffect } from 'react'
import PlaylistCard from '../components/playlistcard'
import TitleBar from '../components/titlebar';

import { getPlaylists } from '../util/api';
import AddPlaylistDialog from '../components/addplaylist';
import { Stack, Typography } from '@mui/joy'

function Home() {
  const [playlistData, setPlaylistData] = useState([]);
  const refreshPlaylists = async () => {
    setPlaylistData(await getPlaylists());
  }

  useEffect(() => {
    refreshPlaylists();
  }, []);

  return (

    <>
      <Typography level="h2">Saved Playlists</Typography>

      <AddPlaylistDialog refreshPlaylists={refreshPlaylists}/>
      <Stack spacing={2} sx={{marginTop: 2}}>
        {playlistData.map(data => 
          <PlaylistCard 
            key={data.id}
            refreshPlaylists={refreshPlaylists}
            {...data} 
          />)
        }
      </Stack>
      
    </>
  )
}

export default Home
