import { Outlet } from "react-router-dom";
import TitleBar from "../components/titlebar";
import { CssVarsProvider } from "@mui/joy/styles";
import { Sheet, Grid, Divider } from '@mui/joy'
import Breadcrumbs from '@mui/joy/Breadcrumbs';
import { Link } from "react-router-dom";


export default function Root() {
    return <CssVarsProvider>
        <Sheet variant="plain">

        
        <Grid container>
            <Grid xs={1} md={2}></Grid>
            <Grid xs={10} md={8}>
                    <TitleBar />
                    <Divider sx={{marginBottom: 2}}/>
                    <div>
                        <Outlet />
                    </div>
            </Grid>
            <Grid xs={1} md={2}></Grid>
        </Grid>
        </Sheet>
    </CssVarsProvider>;
}