import React from 'react'
import ReactDOM from 'react-dom/client'
import '@fontsource/inter'

import {
  createBrowserRouter,
  RouterProvider
} from 'react-router-dom';

import Root from './routes/root.jsx';
import Home from './routes/home.jsx';
import PlaylistView, {loader as playlistLoader} from './routes/playlist.jsx';

import { Link } from 'react-router-dom';
import { Typography } from '@mui/joy';

const router = createBrowserRouter([
  {
    path: '/',
    element: <Root />,
    children: [
      {
        path: "/",
        element: <Home />
      },
      {
        path: "playlists/:playlistId",
        element: <PlaylistView />,
        loader: playlistLoader,
        handle:{
          crumb: (data) => <Typography level="h3">
            <Link 
            to={`/playlists/${data.playlistData.id}`} 
            key={data.playlistData.id}> 
              
                {data.playlistData.title}
              
            </Link>
            </Typography>
        },
      }
    ]
  }
])

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
