const API_ROOT = '/api'

function makePOSTParams(data) {
    return {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    }
}

export async function getPlaylists() {
    const res = await fetch(API_ROOT + '/playlists/');
    return await res.json();
}

export async function getPlaylistData(id) {
    const res = await fetch(API_ROOT + '/playlists/' + id);
    return await res.json();
}

export async function addPlaylist(id) {
    const body = { id }
    const res = await fetch(API_ROOT + '/playlists/', makePOSTParams(body));
    return await res.json();
}

export async function deletePlaylist(id) {
    const res = await fetch(API_ROOT + '/playlists/' + id, {method: 'DELETE'});
    return await res.json();
}

export async function getVideosForPlaylist(id) {
    const res = await fetch(API_ROOT + '/playlists/' + id + '/videos/');
    return await res.json();
}
