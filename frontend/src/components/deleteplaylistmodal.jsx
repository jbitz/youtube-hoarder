
import { 
    Modal, 
    ModalClose, 
    ModalDialog, 
    DialogContent, 
    DialogTitle, 
    Typography,
    Button,
    Box,
    ButtonGroup,
    Checkbox
} from '@mui/joy';

export default function DeletePlaylistModal({open, setOpen, title, numVideos, deleteSelf}) {
    return <Modal open={open} onClose={() => setOpen(false)}>
        <ModalDialog layout="center" size="lg" >
            <ModalClose />
            <DialogTitle>
                Delete Playlist
            </DialogTitle>
            <DialogContent>
                <Typography>
                    Are you sure you want to delete the playlist{' '}
                    <Typography textColor="var(--joy-palette-primary-plainColor)"
                    sx={{fontWeight: 'bold'}}>
                        {title}
                    </Typography>
                    &nbsp;along with{' '}
                    <Typography textColor="var(--joy-palette-primary-plainColor)"
                    sx={{fontWeight: 'bold'}}>
                        {numVideos} videos
                    </Typography>
                    ?
                </Typography>
                <Box sx={{marginTop: 1}}>
                    <Button color="danger" sx={{marginRight: 1}} onClick={() => {
                        setOpen(false);
                        deleteSelf();
                    }}>
                        Yes
                    </Button>
                    <Button color="neutral" onClick={() => setOpen(false)}>
                        No
                    </Button>
                </Box>
            </DialogContent>
        </ModalDialog>
    </Modal>
}