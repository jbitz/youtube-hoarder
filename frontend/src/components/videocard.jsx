import Grid from '@mui/joy/Grid';
import Sheet from '@mui/joy/Sheet';
import Box from '@mui/joy/Box';
import Stack from '@mui/joy/Stack';
import Chip from '@mui/joy/Chip';
import Typography from '@mui/joy/Typography/Typography';
import Button from '@mui/joy/Button';
import Link from '@mui/joy/Link';
import { Divider } from '@mui/joy';

export default function VideoCard({
    id, title, channel, description, thumbnail, status, filepath, playlist_order
}) {
    let thumbnailPath = `/static/thumbnails/${id}.jpg`;
    let mediaPath = `/static/${filepath}`;
    
    let statusString = status;
    if (statusString === 'missing') { // "pending" sounds less like there's a problem
        statusString = 'pending';
    }
    const statusChip = <Chip variant="solid" size="lg" 
        color={
        {missing: 'warning', downloading: 'primary', complete: 'success'}[status]
    }>
        {statusString.substr(0, 1).toUpperCase() + statusString.substr(1)}
    </Chip>;

    return (
    <Sheet>
        <Grid container spacing={2}>
            <Grid md={2} xs={3}>
                <img src={thumbnailPath} style={{width: '100%'}}/>
            </Grid>
            <Grid md={10} xs={9}>
                <Stack spacing={1} sx={{marginTop: 1}}>
                    <Typography level="h3">
                        {playlist_order ? playlist_order : '(Removed)'}. {title}
                    </Typography>
                    <Typography level="h4">{channel}</Typography>
                    <Typography level="h4">Backup Status: {statusChip}</Typography>
                    <Box sx={{paddingBottom: 2}}>
                        <Button component="a"
                            variant="soft" color="primary" sx={{marginRight: 1}}
                            disabled={status !== 'complete'}
                            href={mediaPath}>
                            Download
                        </Button>
                    </Box>
                </Stack>
            </Grid>
        </Grid>
        <Divider sx={{marginTop: 1}}/>
    </Sheet>);
}