import { deletePlaylist, getVideosForPlaylist } from "../util/api";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

import Button from '@mui/joy/Button';
import DeletePlaylistModal from "./deleteplaylistmodal";
import { Sheet, Grid, Stack, Typography, Box, Divider } from '@mui/joy'

export default function PlaylistCard({
    id, 
    title, thumbnail, 
    description, 
    num_videos,
    refreshPlaylists
}) {
    const [videos, setVideos] = useState([]);
    const [deleteModalOpen, setDeleteModalOpen] = useState(false);

    const deleteSelf = async () => {
        await deletePlaylist(id);
        refreshPlaylists();
    }

    let thumbnailPath = `/static/thumbnails/${id}.jpg`

    return <Sheet>
        <Grid container spacing={2}>
            <Grid md={2} xs={3}>
                <img src={thumbnailPath} style={{width: '100%'}}/>
            </Grid>
            <Grid md={10} xs={9}>
                <Stack spacing={1} sx={{marginTop: 1}}>
                    <Typography level="h3">
                        <Link to={`/playlists/${id}`}>{title}</Link>
                    </Typography>
                    <Typography level="h4">
                        {num_videos} videos
                    </Typography>
                    <Box sx={{paddingBottom: 2}}>
                        <Button 
                            color="danger" 
                            onClick={() => setDeleteModalOpen(true)}>
                            Delete
                        </Button>
                    </Box>
                    <Typography component="p"> 
                        {description || '(No description)'} 
                    </Typography>
                </Stack>
                
            </Grid>
        </Grid>
        <Divider sx={{marginTop: 1}}/>
        <DeletePlaylistModal open={deleteModalOpen} 
            setOpen={setDeleteModalOpen}
            deleteSelf={deleteSelf}
            title={title} 
            numVideos={num_videos}
        />
    </Sheet>;
}