import { useState } from "react"
import { addPlaylist } from "../util/api";

import { 
    FormControl, 
    FormLabel, 
    Button, 
    CircularProgress, 
    Input, 
    Box,
    Typography,
} from '@mui/joy';


export default function AddPlaylistDialog({refreshPlaylists}) {
    const [message, setMessage] = useState('');
    const [textInput, setTextInput] = useState('');
    const [buttonEnabled, setButtonEnabled] = useState(true);

    const submitPlaylist = async () => {

        const paramString = textInput.substring(textInput.indexOf('?') + 1);
        const params = new URLSearchParams(paramString);
        const playlistID = params.get('list')

        if (playlistID) {
            setMessage('Downloading playlist info...')
            setButtonEnabled(false);
            const result = await addPlaylist(playlistID);
            if (result.error) {
                setMessage(result.error);
            } else {
                setMessage('Playlist added!');
                refreshPlaylists();
            }
        } else {
            setMessage('Error: Could not add playlist!');
        }
        setButtonEnabled(true);
    }

    return <FormControl>

        <div style={{display: 'flex', alignItems: 'center', gap: 10}}>
            <FormLabel>
                <Typography level="h4">
                Add a new playlist by pasting the link:

                </Typography>
            </FormLabel>
            <Input type="text" 
                onChange={e => setTextInput(e.target.value)} 
                value={textInput}
                endDecorator={
                    <Button disabled={!buttonEnabled} onClick={submitPlaylist}>Add</Button>
                }
                sx={{width: '50%'}}>
            </Input>
            {message && <>
                {!buttonEnabled && <CircularProgress 
                    variant="soft" 
                    size="sm" 
                />}
                <Typography>{message}</Typography>
                </>}
        </div>
    </FormControl>;
}