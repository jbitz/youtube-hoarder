import Typography from '@mui/joy/Typography'
import Navigation from "../components/navigation";
import { Sheet } from '@mui/joy';

export default function TitleBar() {
    return (
        <Sheet sx={{
            paddingTop: 3, 
            paddingBottom: 1,
            display: 'flex',
            gap: 10,
            alignItems: 'center'
        }}>
            <Typography level="h1">
                YouTube Hoarder
            </Typography>
            <Navigation />
        </Sheet>

    );
}