import { Typography } from "@mui/joy";
import Breadcrumbs from "@mui/joy/Breadcrumbs/Breadcrumbs";
import { Link } from "react-router-dom";
import { useMatches } from "react-router-dom";


export default function Navigation() {
    const matches = useMatches();

    let crumbs = matches.filter( (match) => Boolean(match.handle?.crumb))
        .map((match) => match.handle.crumb(match.data));

    return <Breadcrumbs sx={{display: 'inline'}}>
        <Typography level="h3">
            <Link to="/">Home</Link>

        </Typography>
        {crumbs}
    </Breadcrumbs>
}