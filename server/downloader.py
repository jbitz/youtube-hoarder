from __future__ import unicode_literals
from googleapiclient.discovery import build

import os
import youtube_dl
import requests
import click
import db

MEDIA_LOCATION = os.environ['MEDIA_LOCATION']
GOOGLE_API_KEY = os.environ['GOOGLE_API_KEY']

if not os.path.isdir(MEDIA_LOCATION):
    os.mkdir(MEDIA_LOCATION)

# Initialize the YouTube API client
youtube = build('youtube', 'v3', developerKey=GOOGLE_API_KEY)

def get_video_info(video_id, should_download_thumbnail=True):
    request = youtube.videos().list(
        part='snippet',
        id=video_id
    )
    response = request.execute()

    if response['pageInfo']['totalResults'] != 1:
        click.echo(f'Error: Invalid video ID {video_id}', err=True)
        return None
    
    video_info = response['items'][0]['snippet']
    video = {
        'id': video_id,
        'title': video_info['title'],
        'channel': video_info['channelTitle'],
        'description': video_info['description'],
        'thumbnail': video_info['thumbnails']['high']['url']
    }

    if should_download_thumbnail:
        download_thumbnail(video)

    return video

def get_playlist_info(playlist_id, should_download_thumbnail=True):
    """Gets the metadata for the YouTube playlist with the given ID"""
    request = youtube.playlists().list(
        part='snippet',
        id=playlist_id
    )
    response = request.execute()

    if response['pageInfo']['totalResults'] != 1:
        click.echo(f'Error: Invalid playlist ID {playlist_id}', err=True)
        return None
    
    playlist_info = response['items'][0]['snippet']
    playlist = {
        'id': playlist_id,
        'title': playlist_info['title'],
        'description': playlist_info['description'],
        'thumbnail': playlist_info['thumbnails']['high']['url'],
        'channel-title': playlist_info['channelTitle']
    }

    if should_download_thumbnail:
        download_thumbnail(playlist)
    
    return playlist


def get_playlist_video_ids(playlist_id):
    """Gets a list of all video IDs in this playlist"""

    playlist_items = []
    next_page_token = None
    try:
        while True:
            # Call the API to retrieve playlist items
            request = youtube.playlistItems().list(
                part='contentDetails',
                playlistId=playlist_id,
                maxResults=50,  # Maximum allowed value
                pageToken=next_page_token
            )
            response = request.execute()

            # Append videos to the list
            for item in response['items']:
                playlist_items.append(item['contentDetails']['videoId'])

            # Check for next page token
            next_page_token = response.get('nextPageToken')
            if not next_page_token:
                break  # No more pages
    except:
        return []

    return playlist_items

# TODO: Deal with case where playlist already exists
# TODO: Download playlist thumbnail locally
def add_playlist(id):    
    info = get_playlist_info(id)
    if not info:
        click.echo(f'Could not find playlist with ID {id}', err=True)
        return
    
    db.add_playlist(info)
    return db.get_playlist_by_id(id)
    

def check_for_new_videos():
    """Checks all playlists in the database for new videos"""
    playlists = db.get_playlists()
    for playlist in playlists:
        check_playlist_for_new_videos(playlist)
    

# TODO: Download video thumbnail locally
def check_playlist_for_new_videos(playlist):
    """Checks the given playlist against the database and adds any new ones.
    Should only be called if this playlist is already in the database.

    Use playlist=None to check for videos without a playlist

    Set download=False to just add videos to DB without downloading them
    """
    click.echo(f"Checking playlist {playlist['id']} ({playlist['title']}) for new videos")
    
    local_videos = {vid['id'] for vid in db.get_videos_by_playlist(playlist['id'])}
    online_videos = get_playlist_video_ids(playlist['id'])

    # Keep the same order as the original
    for idx, video_id in enumerate(online_videos):
        if video_id not in local_videos: # need to download the info
            info = get_video_info(video_id)
            # Will be null if the video, for example, has been made unavailable
            if info:
                db.add_video(info, playlist_id=playlist['id'])
        db.set_video_playlist_order(video_id, idx + 1) # index from 1

    # Any videos which were stored before, but have since been removed from the playlist
    # Set their order column to NULL
    # Don't do this if there were NO online videos, which probably means that the playlist
    # has since been deleted. If that's the case, just keep the old order that we had stored
    if len(online_videos) > 0:
        for video_id in local_videos - set(online_videos):
            db.set_video_playlist_order(video_id, None)

    # Add all the new videos to the download queue    
    undownloaded = db.get_videos_by_playlist_and_status(playlist['id'], 'missing')
    for video in undownloaded:
        db.add_video_to_download_queue(video['id'])

def download_thumbnail(obj):
    """Downloads the thumbnail for the given video or playlist object"""
    thumbnail_dir = os.path.join(MEDIA_LOCATION, 'thumbnails')
    if not os.path.isdir(thumbnail_dir):
        os.mkdir(thumbnail_dir)

    res = requests.get(obj['thumbnail'])
    if res.ok:
        filepath = os.path.join(thumbnail_dir, f'{obj["id"]}.jpg')
        if os.path.exists(filepath):
            os.remove(filepath)
        with open(filepath, 'xb') as f:
            f.write(res.content)
    else:
        click.echo('Error: Could not download')