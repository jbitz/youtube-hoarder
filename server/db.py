import sqlite3
import click
import os
from flask import g

db = None

def set_db(db_connection):
    global db
    db = db_connection

"""TODO: Deal with case of video appearing in multiple playlists"""

def init_db():
    cur = db.cursor()
    cur.execute("""
        CREATE TABLE IF NOT EXISTS playlists (
            id TEXT PRIMARY KEY,
            title TEXT NOT NULL,
            description TEXT,
            thumbnail_url TEXT
        );
    """)
    cur.execute("""
        CREATE TABLE IF NOT EXISTS videos (
            id TEXT NOT NULL,
            title TEXT NOT NULL,
            channel TEXT NOT NULL,
            description TEXT,
            thumbnail_url TEXT,
            playlist_id TEXT,
            status TEXT NOT NULL,
            filepath TEXT,
            playlist_order INTEGER,
            FOREIGN KEY (playlist_id) REFERENCES playlists(id)
        );
    """)
    cur.execute("""
        CREATE TABLE IF NOT EXISTS download_queue (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            video_id INTEGER,
            timestamp INTEGER,
            FOREIGN KEY (video_id) REFERENCES videos(id)        
        )
    """)
    # the status column is one of "missing", "downloading", or "complete"

def row_to_playlist(row):
    """Converts a raw row from sqlite (just a tuple) into a dictionary with named keys"""

    if row is None:
        return None
    
    return {
        'id': row[0],
        'title': row[1],
        'description': row[2],
        'thumbnail': row[3],
        'num_videos': row[4]
    }

def row_to_video(row):
    if row is None:
        return None
    
    return {
        'id': row[0],
        'title': row[1],
        'channel': row[2],
        'description': row[3],
        'thumbnail': row[4],
        'playlist_id': row[5],
        'status': row[6],
        'filepath' : row[7],
        'playlist_order': row[8],
    }

def add_video(video, playlist_id=None):
    id, title, channel, description, thumbnail = \
        video['id'], video['title'], video['channel'], video['description'], video['thumbnail']
    
    cur = db.cursor()
    cur.execute("""
        INSERT INTO videos (id, title, channel, description, thumbnail_url, playlist_id, status)
        VALUES (?, ?, ?, ?, ?, ?, 'missing')
    """, [id, title, channel, description, thumbnail, playlist_id])
    db.commit()
    click.echo(f'Added video {id} - {title} to database.')

def get_video_by_id(video_id):
    cur = db.cursor()
    result = cur.execute("SELECT * FROM videos WHERE id=?", [video_id]).fetchone()
    return row_to_video(result)

def get_videos_by_playlist(playlist_id):
    cur = db.cursor()
    result = cur.execute(
        "SELECT * FROM videos WHERE playlist_id=? ORDER BY playlist_order;", 
        [playlist_id]
    ).fetchall()
    return [row_to_video(row) for row in result]

def get_videos_by_playlist_and_status(playlist_id, status):
    cur = db.cursor()
    result = cur.execute(
        "SELECT * FROM videos WHERE playlist_id=? AND status=?;", 
        [playlist_id, status]
    ).fetchall()
    return [row_to_video(row) for row in result]

def set_video_status(video_id, status):
    if status not in ['missing', 'downloading', 'complete']:
        click.echo(f'Error: Invalid video status {status}', err=True)
        return
    
    cur = db.cursor()
    cur.execute(
        "UPDATE videos SET status=? WHERE id=?", 
        [status, video_id]
    )
    db.commit()
    click.echo(f'Updated status of video {video_id} to {status}')

def set_video_playlist_order(video_id, order):
    cur = db.cursor()
    cur.execute('UPDATE videos SET playlist_order = ? WHERE id = ?', [order, video_id])
    db.commit()

def set_video_location(video_id, filename):
    cur = db.cursor()
    cur.execute(
        "UPDATE videos SET filepath=? WHERE id=?",
        [filename, video_id]
    )
    db.commit()
    click.echo(f'Updated filepath of video {video_id} to {filename}')

def get_playlists():
    cur = db.cursor()
    result = cur.execute("""
        SELECT p.*, COUNT(v.id) FROM playlists AS p LEFT JOIN videos AS v
        ON p.id = v.playlist_id
        GROUP BY p.id
        ORDER BY p.title;
    """).fetchall()
    return [row_to_playlist(row) for row in result]

def get_playlist_by_id(playlist_id):
    cur = db.cursor()
    result = cur.execute("""
        SELECT p.*, COUNT(v.id) FROM playlists AS p LEFT JOIN videos AS v
        ON p.id = v.playlist_id
        WHERE p.id = ?
        GROUP BY p.id;
    """, [playlist_id]).fetchone()
    return row_to_playlist(result) if result else None

def add_playlist(playlist):
    id, title, description, thumbnail = \
        playlist['id'], playlist['title'], playlist['description'], playlist['thumbnail']
    
    cur = db.cursor()
    try:
        cur.execute("""
            INSERT INTO playlists (id, title, description, thumbnail_url) 
            VALUES (?, ?, ?, ?)
        """, [id, title, description, thumbnail])
        db.commit()
        click.echo(f'Added playlist {id} to database.')

    except sqlite3.IntegrityError as e:
        click.echo(f'Error: Attempted to add duplicate playlist {id} to database!', err=True)

def delete_playlist(playlist_id):
    """Deletes the playlist with the given ID and returns the information from
    the related videos which were also deleted"""
    
    videos = get_videos_by_playlist(playlist_id)

    cur = db.cursor()
    cur.execute('DELETE FROM videos WHERE playlist_id = ?;', [playlist_id])
    cur.execute('DELETE FROM playlists WHERE id = ?;', [playlist_id])
    db.commit()

    click.echo(f'Deleted playlist {playlist_id} along with {len(videos)} videos:')
    for idx, v in enumerate(videos):
        click.echo(f"{idx + 1}) {v['id']} - {v['title']}")

    return videos

def add_video_to_download_queue(video_id, redownload=False):
    """Adds the given video id to the download queue. If redownload is true,
    deletes the old video file and schedules it to redownload from scratch. If 
    redownload is false and the video has already been downloaded, do nothing."""

    cur = db.cursor()
    video = get_video_by_id(video_id)
    if video is None:
        click.echo(f"Error: Attempted to queue nonexistant video {video_id}", err=True)
        return
    
    if video['status'] == 'complete':
        if redownload:
            raise NotImplemented("You need to finish this code path!")
        else:
            return

    try:
        cur.execute(
            "INSERT INTO download_queue (video_id, timestamp) VALUES (?, unixepoch());", 
            [video_id]
        )
        db.commit()
    except sqlite3.IntegrityError as e:
        click.echo(f'Error: Error adding video {video_id} to download queue!', err=True)

def get_next_from_queue():
    """Returns the next video object in the download queue, or None if it doesn't match"""
    cur = db.cursor()
    res = cur.execute("""
        SELECT * FROM videos WHERE id = 
            (SELECT video_id FROM download_queue ORDER BY timestamp LIMIT 1);
    """).fetchone()
    res = row_to_video(res)

    if res is not None:
        cur.execute("DELETE FROM download_queue WHERE video_id=?;", [res['id']])
        db.commit()
    return res