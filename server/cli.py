from dotenv import load_dotenv
load_dotenv()

import downloader
import db
import os
import sqlite3
import click

@click.group()
def main():
    pass

@main.command()
def init():
    db.init_db()
    click.echo('Initialized DB')

# --- Playlist commands
@main.group()
def playlist():
    pass

@playlist.command()
@click.argument('id')
def add(id):
    downloader.add_playlist(id)

@playlist.command()
@click.argument('id')
def list_items(id):
    click.echo(f'Items in playlist {id}')
    items = downloader.get_playlist_video_ids(id)
    click.echo(items)
    
@playlist.command()
@click.argument('id')
def info(id):
    info = db.get_playlist_by_id(id)
    if not info:
        info = downloader.get_playlist_info(id)
        click.echo('Playlist NOT saved locally:')
    else:
        click.echo('Playlist saved locally:')
    if info:
        for key in info:
            click.echo(f'{key}: {info[key]}')

@playlist.command()
def list():
    playlists = db.get_playlists()
    for pl in playlists:
        print(f"{pl['title']}\t\t{pl['id']}")

@playlist.command()
@click.option('--id', default="", help="ID of playlist (omit to sync all playlists)")
def sync(id):
    if id:
        playlist = db.get_playlist_by_id(id)
        if not playlist:
            click.echo('Playlist not know locally, attempting to add')
            playlist = downloader.add_playlist(id)
            if not playlist:
                return

        downloader.check_playlist_for_new_videos(playlist)
    else:
        downloader.check_for_new_videos()
    
#-------- Video commands
@main.group()
def video():
    pass

@video.command()
@click.argument('id')
def info(id):
    info = downloader.get_video_info(id)
    for key in info:
        click.echo(f'{key}: {info[key]}')

@video.command()
@click.argument('id')
def add(id):
    info = downloader.get_video_info(id)
    if not info:
        click.echo(f'Could not find video with id {id}', err=True)
        return

    db.add_video(info)

if __name__ == '__main__':
    DB_FILE = os.path.join(os.environ['DATA_LOCATION'], 'db.sqlite')
    db_connection = sqlite3.connect(DB_FILE)
    db.set_db(db_connection)
    main()