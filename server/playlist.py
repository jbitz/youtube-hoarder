from flask import Blueprint, request
import db
import downloader

playlists = Blueprint('playlists', __name__)

@playlists.route('/')
def get_playlists():
    return db.get_playlists()

@playlists.route('/<id>')
def get_playlist(id):
    res = db.get_playlist_by_id(id)
    if res:
        return res
    else:
        return {
            'error': f'Playlist {id} not found'
        }
    
@playlists.route('/', methods=['POST'])
def add_playlist():
    if db.get_playlist_by_id(request.json['id']):
        return {
            'error': 'Playlist already added!'
        }

    playlist = downloader.add_playlist(request.json['id'])
    if not playlist:
        return {
            'error': 'Error: Could not find playlist'
        }
    else:
        downloader.check_playlist_for_new_videos(playlist)
        return {
            'message': 'Success',
            'playlist': playlist
        }
    
@playlists.route('/<id>', methods=['DELETE'])
def delete_playlist(id):
    if not db.get_playlist_by_id(id):
        return {
            'error': 'Playlist does not exist!'
        }
    
    db.delete_playlist(id)
    return {
            'message': 'Success',
    }

@playlists.route('/<id>/videos/')
def get_videos_by_playlist(id):
    if not db.get_playlist_by_id(id):
        return {
            'error': 'Playlist does not exist!'
        }
    
    return db.get_videos_by_playlist(id)