"""
Watches the download queue for new videos and downloads them when they appear
"""


from dotenv import load_dotenv
load_dotenv()

import click
import db
import youtube_dl
import time
import os
import sqlite3

MEDIA_LOCATION = os.environ['MEDIA_LOCATION']
DB_FILE = os.path.join(os.environ['DATA_LOCATION'], 'db.sqlite')
SIMULATE_DOWNLOAD = bool(os.environ.get('SIMULATE_DOWNLOAD'))

def download_next(undownloaded_vid):
    vid_id = undownloaded_vid['id']
    db.set_video_status(vid_id, 'downloading')
    filename = download_from_youtube(vid_id)
    if filename:
        db.set_video_status(vid_id, 'complete')
        db.set_video_location(vid_id, filename)
    else:
        db.set_video_status(vid_id, 'missing')
        click.echo(f'Error: Unknown error when downloading video {vid_id}')

def get_filename_after_download(video_id):
    """This is a janky way to do it until I read more of the youtube-dl code to
    find out how to get back the filename after a download"""

    files = os.listdir(MEDIA_LOCATION)
    for file in files:
        if file.startswith(video_id):
            return file
        
    click.echo(f"Error: Tried to get file for video {video_id}, but it doesn't exist!")
    return None

def download_from_youtube(video_id):
    """Downloads the video with the given ID. Returns the path to the file"""
    opts = {
        'outtmpl': f'{MEDIA_LOCATION}/%(id)s-%(title)s',
        'quiet': False,
        'getfilename': True,
        'simulate': SIMULATE_DOWNLOAD,
    }
    url = f'https://www.youtube.com/watch?v={video_id}'
    with youtube_dl.YoutubeDL(opts) as ydl:
        ydl.download([url])
    return get_filename_after_download(video_id)

def main():
    db_connection = sqlite3.connect(DB_FILE)
    db.set_db(db_connection)

    while True:
        next_vid = db.get_next_from_queue()
        if next_vid:
            download_next(next_vid)
        else:
            time.sleep(5) # Wait 5 seconds before checking again

if __name__ == '__main__':
    main()