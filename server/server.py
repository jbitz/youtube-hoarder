from dotenv import load_dotenv
load_dotenv()

import os
from flask import Flask, g, send_file
from flask_cors import CORS

import sqlite3
import db

from playlist import playlists

MEDIA_LOCATION = os.environ['MEDIA_LOCATION']
app = Flask(__name__, static_folder=MEDIA_LOCATION, static_url_path='/static')
CORS(app)

# Handle database connection before each request
DB_FILE = os.path.join(os.environ['DATA_LOCATION'], 'db.sqlite')
@app.before_request
def connect_db():
    if 'db' not in g:
        g.db = sqlite3.connect(DB_FILE)
    db.set_db(g.db)

@app.teardown_appcontext
def close_db(e=None):
    db_conn = g.pop('db', None)
    if db_conn is not None:
        db_conn.close()
    db.set_db(None)


# Handle static files
@app.route('/')
def index():
    return send_file('fe_static/index.html')

@app.route('/assets/<path>')
def static_asset(path):
    # Directory where frontend static files are copied after build
    return send_file(os.path.join('fe_static', 'assets', path))

app.register_blueprint(playlists, url_prefix='/api/playlists')
