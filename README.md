# YouTube Hoarder

YouTube Hoarder is a tool that allows you to preserve playlists of videos on YouTube in the event that they are taken down from the platform.

<img src="doc/playlist_view.png" height="500"/>

## Setup

This application is intended to be run in a Docker container. It requires
two directories for media and metadata storage - by default, these are mounted
at `/media` and `/data` within the container. Change the `/path/to/media/location`
and `/path/to/data/location` portions of the command below to match your hosting environment.

You will also need a Google API key (used for pulling metadata from YouTube).
You can follow [these instructions](https://developers.google.com/youtube/v3/getting-started) to create a key to use with the YouTube API.

The web interface is exposed on port 5000.

```sh
docker run -p 5000:5000 \
-v /path/to/media/location:/media \
-v /path/to/data/location:/data \
--env DATA_LOCATION=/data \
--env MEDIA_LOCATION=/media \
--env GOOGLE_API_KEY=YOUR_API_KEY \
registry.gitlab.com/jbitz/youtube-hoarder:latest
```

## Usage
Once the application is up and running, access the web interface in a browser
and paste in the link to a YouTube playlist (it can be either a public or unlisted
playlist). The application will automatically download all videos currently in the playlist. After that, it checks every hour for new videos in each registered playlist
and downloads them.

Logs are output in a the file `log.log` under the data directory.